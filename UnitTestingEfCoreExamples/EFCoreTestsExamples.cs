﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.EntityFrameworkCore;

namespace UnitTestingEfCoreExamples
{
    public class BookContext : DbContext
    {
        public BookContext(DbContextOptions options) : base(options)
        {
            
        }
        
        public BookContext() {}
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "testDatabase");
            //optionsBuilder.UseSqlite("Data Source=BookTest.db");
            base.OnConfiguring(optionsBuilder);
        }

        public virtual DbSet<Book> Books { get; set; }
    }

    public class Book
    {
        public int Id { get; set; }
        
        [Required]
        public string Title { get; set; }

        
        [Required]
        public string Author { get; set; }

        public DateTime DatePublished { get; set; }
        

        public Book Sequel { get; set; }
        
        public Book()
        {
            DatePublished = new DateTime();
        }
    }

    public class BookRepository
    {
        private BookContext _db;

        public BookRepository(BookContext db)
        {
            _db = db;
        }

        public void AddBook(string title, string author)
        {
            _db.Books.Add(new Book() {Title = title, Author = author});
            _db.SaveChanges();
        }

        public List<Book> GetBooksByAuthor(string title)
        {
            return _db.Books.AsNoTracking()
                .Where(x => string.Equals(x.Author, title, StringComparison.CurrentCultureIgnoreCase))
                .ToList();
        }

        public List<Book> FetchBooks()
        {
            var result = _db.Books.ToList();
            return result;
        }

        public List<Book> GetBooksWithSequelTitle(string sequelTitle)
        {
            return _db.Books.Include(x => x.Sequel).Where(x =>
                    x.Sequel != null &&
                    string.Equals(x.Sequel.Title, sequelTitle, StringComparison.CurrentCultureIgnoreCase))
                .ToList();
        }
    }

    [TestClass]
    public class BookRespositoryTest
    {
        [TestMethod]
        public void FetchBookList()
        {
            var books = new List<Book>()
            {
                new Book
                {
                    Title = "Hamlet",
                    Author = "William Shakespeare"
                },
                new Book
                {
                    Title = "A Midsummer Night's Dream",
                    Author = "William Shakespeare"
                }
            };
            
            var mockContext = new Mock<BookContext>();
            mockContext.Setup(context => context.Books).ReturnsDbSet(books);            
            
            //act
            var repository = new BookRepository(mockContext.Object);
            var actual = repository.FetchBooks();
            
            //assert
            Assert.AreEqual(2, actual.Count());
            Assert.AreEqual("Hamlet", actual.First().Title);
        }
        
        [TestMethod]
        public void FetchBookList2()
        {
            IQueryable<Book> books = new List<Book>()
            {
                new Book
                {
                    Title = "Hamlet",
                    Author = "William Shakespeare"
                },
                new Book
                {
                    Title = "A Midsummer Night's Dream",
                    Author = "William Shakespeare"
                }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Book>>();
            mockSet.As<IQueryable<Book>>().Setup(m => m.Provider).Returns(books.Provider);
            mockSet.As<IQueryable<Book>>().Setup(m => m.Expression).Returns(books.Expression);
            mockSet.As<IQueryable<Book>>().Setup(m => m.ElementType).Returns(books.ElementType);
            mockSet.As<IQueryable<Book>>().Setup(m => m.GetEnumerator()).Returns(books.GetEnumerator());

            var mockContext = new Mock<BookContext>();
            mockContext.Setup(context => context.Books).Returns(mockSet.Object);
            
            //act
            var repository = new BookRepository(mockContext.Object);
            var actual = repository.FetchBooks();
            
            //assert
            Assert.AreEqual(2, actual.Count());
            Assert.AreEqual("Hamlet", actual.First().Title);
        }

        [TestMethod]
        public void CreateBookTest()
        {
            int callCount = 0;
            int saveChanges = 0;
            
            // Arrange - We're mocking our dbSet & dbContext
            // in-memory implementations of you context and sets
            var mockSet = new Mock<DbSet<Book>>();
            mockSet.Setup(m => m.Add(It.IsAny<Book>()));
            

            var mockContext = new Mock<BookContext>();
            mockContext.Setup(x => x.Books).Returns(mockSet.Object);
            //mockContext.Setup(x => x.SaveChanges()).Returns(mockContext.Object);
            
//            mockContext.Setup(m => m.Books).Returns(mockSet.Object);
            mockContext.Setup(x => x.SaveChanges()).Callback(() =>
            {
                callCount++;
            });
            
            
//            var context = new BookContext() { };

            // Act - Add the book
//            var repository = new BookRepository(mockContext.Object);
            var repository = new BookRepository(mockContext.Object);
            repository.AddBook("Macbeth", "William Shakespeare");

            // Assert
            // These two lines of code verifies that a book was added once and
            // we saved our changes once.
            mockSet.Verify(m => m.Add(It.IsAny<Book>()), Times.Once);
            Assert.AreEqual(1, callCount);
            mockContext.Verify(m => m.SaveChanges(), Times.Once);
        }

        [TestMethod]
        public void GetBooksByAuthor_WithDifferentCase()
        {
            var expected = new List<Book>();
            expected.Add(new Book
            {
                Title = "Hamlet",
                Author = "William Shakespeare"
            }); 
            expected.Add(new Book
            {
                Title = "A Midsummer Night's Dream",
                Author = "William Shakespeare"
            });
            
            //arrange
            var books = new List<Book>()
            {
                expected[0],
                expected[1],
                new Book
                {
                    Title = "Test",
                    Author = "West"
                }
            };
            
            var mockContext = new Mock<BookContext>();
            mockContext.Setup(context => context.Books).ReturnsDbSet(books);            
            
            //act
            var repository = new BookRepository(mockContext.Object);
            var actual = repository.GetBooksByAuthor("william shakespeare");
            
            //assert
            Assert.AreEqual(2, actual.Count());
            CollectionAssert.AreEquivalent(expected, actual);
            //Assert.AreEqual("Hamlet", actual.First().Title);            
        }
        
        [TestMethod]
        public void GetBooksByAuthor_WithCorrectCase()
        {
            var expected = new List<Book>();
            expected.Add(new Book
                {
                    Title = "Hamlet",
                    Author = "William Shakespeare"
                }); 
            expected.Add(new Book
                {
                    Title = "A Midsummer Night's Dream",
                    Author = "William Shakespeare"
                });
            
            //arrange
            var books = new List<Book>()
            {
                expected[0],
                expected[1],
                new Book
                {
                    Title = "Test",
                    Author = "West"
                }
            };
            
            var mockContext = new Mock<BookContext>();
            mockContext.Setup(context => context.Books).ReturnsDbSet(books);            
            
            //act
            var repository = new BookRepository(mockContext.Object);
            var actual = repository.GetBooksByAuthor("William Shakespeare");
            
            //assert
            Assert.AreEqual(2, actual.Count());
            CollectionAssert.AreEquivalent(expected, actual);
            //Assert.AreEqual("Hamlet", actual.First().Title);
        }

        [TestMethod]
        public void GetSequelByName()
        {
            var expected = new List<Book>();
            var sequel = new Book
            {
                Title = "A Midsummer Night's Dream",
                Author = "William Shakespeare"
            };

            var hamlet = new Book
            {
                Title = "Hamlet",
                Author = "William Shakespeare",
                Sequel = sequel
            };
            expected.Add(hamlet); 
            expected.Add(sequel);
            
            //arrange
            var books = new List<Book>()
            {
                expected[0],
                expected[1],
                new Book
                {
                    Title = "Test",
                    Author = "West"
                }
            };
            
            var mockContext = new Mock<BookContext>();
            mockContext.Setup(context => context.Books).ReturnsDbSet(books);            
            
            //act
            var repository = new BookRepository(mockContext.Object);
            var actual = repository.GetBooksWithSequelTitle("A Midsummer Night's Dream");
            
            //assert
            Assert.AreEqual(1, actual.Count());
            CollectionAssert.AreEquivalent( new[] {hamlet}, actual);
        }
        
        
    }
    
}