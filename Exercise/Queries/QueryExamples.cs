using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Exercise.Queries
{
    public class QueryExamples
    {
        #region Queries

        public static List<Team> RecreantenTeams(VerenigingContext db)
        {
            //team aanmaken dat aan voorwaarden voldoet
            Team t = db.Teams.Skip(3).Take(1).First();
            foreach (var wedstrijdSpeler in t.Spelers)
            {
                wedstrijdSpeler.Geboortedag = new DateTime(1950, 1, 1);
                wedstrijdSpeler.Toetreding = DateTime.Now;
            }

            db.SaveChanges();            
            
            //de SQL gebruikt geen SQL All, (kan aan SQLite liggen en/of Age methode)
            var teams = db.Teams.Include(x => x.Spelers)
                .Where(x => x.Spelers.All(w => Age(w.Geboortedag, w.Toetreding) > 30));
            var res = teams.ToList();
            var result = new List<Team>();
            return result;
        }

        private static int Age(DateTime bday, DateTime refDate)
        {
            DateTime now = refDate;
            int age = now.Year - bday.Year;
            if (bday > now.AddYears(-age)) age--;
            return age;
        }

        public static Team TeamMetMeesteWedstrijden(VerenigingContext db)
        {
            var q1 = db.Teams.Select(x => new {Cnt = x.Wedstrijden.Count(), Team = x})
                .Where(x => x.Cnt == db.Teams.Max(w => w.Wedstrijden.Count()))
                .OrderByDescending(x => x.Cnt).ToList();

            // var q2 = db.Wedstrijden.GroupBy(x => x.Team)
            //     .OrderByDescending(x => x.Count())
            //     .First();
            var team = q1.First().Team; 
            return team;
        }

        public static List<Lid> ScheidsVoorZaal(VerenigingContext db, string zaalNaam)
        {
            var q = db.Wedstrijden
                .Where(x => x.Zaal.Naam == zaalNaam)
                .Select(x => x.Scheidsrechter)
                .Distinct();

            return q.ToList();
        }

        public static void RemoveLid(VerenigingContext db)
        {
            var lid = db.Leden.Skip(3).Take(1).First();
            db.Remove(lid);
            db.SaveChanges();

            var trainer = db.Leden.Where(x => x.Traint != null).First();
            db.Remove(trainer);
            //db.Trainingen.Remove(trainer);
            db.SaveChanges();

            //als een team wordt verwijderd dan worden er heel veel verwijderd.
            //zie Queries!
            var team6 = db.Teams.Find(6);
            db.Remove(team6);
            db.SaveChanges();
        }

        #endregion
    }
}