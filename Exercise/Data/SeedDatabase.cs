using System;
using System.Collections.Generic;
using System.Linq;
using Bogus;
using NodaTime;
using NodaTime.Extensions;

namespace Exercise
{
    public class SeedDatabase
    {
        private const int NumLeden = 100;
        private const int NumTeam = 10;
        private const int MinNumWedstrijdspelersInTeam = 15;
        private const int MaxNumWedstrijdspelersInTeam = 20;
        private const int MaxTrainingenPerWeek = 3;
        private const int NumZalen = 5;
        private const int MaxNumBoetesWedstrijd = 5;
        private const int MaxNumWedstrijden = 20;


        private enum Klasse
        {
            competitie_team_heren,
            competitie_team_dames,
            senioren_dames,
            senioren_heren,
            dames_recreaten,
            jeugd
        }

        public static (List<Lid>, List<Team>) GenerateData()
        {
            Randomizer.Seed = new Random(1024);

            var zalen = new Faker<Zaal>()
                .RuleFor(x => x.Naam, (faker, zaal) => $"{faker.Company.CompanyName()} Zaal")
                .RuleFor(x => x.Adres, (faker, zaal) => faker.Address.FullAddress())
                .RuleFor(x => x.Telnr, (faker, zaal) => faker.Phone.PhoneNumber())
                .Generate(NumZalen);

            var trainingen = new Faker<Training>()
                .Rules((faker, training) =>
                {
                    int trainingMinutes = faker.PickRandom(new int[] {45, 60, 90, 120, 150});
                    var startHour = faker.Random.Int(14, 22);
                    var minutesToAdd = faker.PickRandom(new[] {15, 30, 45});

                    var startDay = faker.PickRandom<DayOfWeek>();
                    var startTime = LocalTime.Midnight.PlusHours(startHour).PlusMinutes(minutesToAdd);
                    var endTime = startTime.Plus(Period.FromMinutes(trainingMinutes));

                    training.StartDay = startDay;
                    training.StartTime = startTime;
                    training.EndTime = endTime;
                })
                .RuleFor(x => x.Aanvang, (faker, training) => faker.Date.Past())
                .RuleFor(x => x.Zaal, (faker, training) =>
                {
                    var zaal = faker.PickRandom(zalen);
                    zaal.Trainingen.Add(training);
                    return zaal;
                });

            var leden = new FakerLid().Generate(NumLeden);

            var boetes = new Faker<Boete>()
                .RuleFor(x => x.Bedrag, (faker, boete) => faker.Random.Int(50, 100))
                .RuleFor(x => x.Toelichting, (faker, boete) => faker.Lorem.Sentences(3));

            var wedstrijden = new Faker<Wedstrijd>()
                .RuleFor(x => x.Zaal, (faker, wedstrijd) =>
                {
                    var zaal = faker.PickRandom(zalen);
                    zaal.Wedstrijden.Add(wedstrijd);
                    return zaal;
                })
                .RuleFor(x => x.Scheidsrechter, (faker, wedstrijd) =>
                {
                    var result = faker.PickRandom(leden);
                    result.GeflotenWedstrijden.Add(wedstrijd);
                    return result;
                })
                .RuleFor(x => x.Boetes,
                    (faker, wedstrijd) =>
                    {
                        var res = boetes.Generate(faker.Random.Int(0, MaxNumBoetesWedstrijd));
                        res.ForEach(x => x.Wedstrijd = wedstrijd);
                        return res;
                    });

            //generated team with wedstrijdspelers
            var teams = new Faker<Team>()
                .RuleFor(x => x.TeamCode, (faker, team) => "T" +faker.UniqueIndex.ToString())
                .RuleFor(x => x.Klasse, (faker, team) => faker.PickRandom<Klasse>().ToString())
                .RuleFor(x => x.Spelers, (faker, team) =>
                {
                    var result = new FakerWedstrijdspeler()
                        .Generate(faker.Random.Int(MinNumWedstrijdspelersInTeam, MaxNumWedstrijdspelersInTeam));
                    foreach (var wedstrijdSpeler in result)
                    {
                        wedstrijdSpeler.Team = team;
                    }
                    return result;
                })
                .RuleFor(x => x.Trainer, (faker, team) =>
                {
                    var lid = faker.PickRandom(leden);
                    lid.Traint = team;
                    return lid;
                })
                .RuleFor(x => x.Trainingen, (faker, team) =>
                {
                    var result = trainingen.Generate(faker.Random.Int(1, MaxTrainingenPerWeek));
                    //result.ForEach(x => x.Team = team);
                    return result;
                })
                .RuleFor(x => x.Wedstrijden, (faker, team) =>
                {
                    var result = wedstrijden.Generate(faker.Random.Int(0, MaxNumWedstrijden));
                    foreach (var wedstrijd in result)
                    {
                        //wedstrijd.Team = team;
                        foreach (var boete in wedstrijd.Boetes)
                        {
                            boete.WedstrijdSpeler = faker.Random.Float() < 0.5 ? faker.PickRandom(team.Spelers) : null;
                            if (boete.WedstrijdSpeler != null)
                            {
                                boete.WedstrijdSpeler.Boetes.Add(boete);
                            }
                        }
                    }

                    return result;
                })
                .Generate(NumTeam);

            return (leden, teams);
        }

        public class FakerLid : Faker<Lid>
        {
            public FakerLid()
            {
                RuleFor(x => x.Naam, (faker, lid) => faker.Person.FullName);
                RuleFor(x => x.Adres, (faker, lid) => faker.Address.FullAddress());
                RuleFor(x => x.Geboortedag, (faker, lid) => faker.Date.Past(12).Date);
                RuleFor(x => x.Geslacht, (faker, lid) => faker.PickRandom<Geslacht>());
                RuleFor(x => x.Toetreding,
                    (faker, lid) => faker.Date.Between(lid.Geboortedag.AddYears(6), DateTime.Today).Date);
            }
        }

        public class FakerWedstrijdspeler : Faker<WedstrijdSpeler>
        {
            public FakerWedstrijdspeler() : base()
            {
                RuleFor(x => x.Naam, (faker, lid) => faker.Person.FullName);
                RuleFor(x => x.Adres, (faker, lid) => faker.Address.FullAddress());
                RuleFor(x => x.Geboortedag, (faker, lid) => faker.Date.Past(12).Date);
                RuleFor(x => x.Geslacht, (faker, lid) => faker.PickRandom<Geslacht>());
                RuleFor(x => x.Toetreding,
                    (faker, lid) => faker.Date.Between(lid.Geboortedag.AddYears(6), DateTime.Today).Date);               
                
                RuleFor(x => x.Bodsnummer, (faker, speler) => faker.UniqueIndex);
            }
        }
    }
}