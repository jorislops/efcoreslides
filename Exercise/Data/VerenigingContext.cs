﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

// using Microsoft.Extensions.Logging.Console;

namespace Exercise
{
    public class VerenigingContext : DbContext
    {
        public DbSet<Lid> Leden { get; set; }
        public DbSet<WedstrijdSpeler> WedstrijdSpelers { get; set; }
        public DbSet<Wedstrijd> Wedstrijden { get; set; }
        public DbSet<Training> Trainingen { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Boete> Boetes { get; set; }
        public DbSet<Zaal> Zalen { get; set; }
        
        //    public DbQuery<ZaalRooster> ZaalRoosters { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
            
            Console.WriteLine("test");

            optionsBuilder.UseMySQL("server=127.0.0.1;uid=root;pwd=Test@1234!;database=vereniging");

            // optionsBuilder.UseSqlite("Data Source=Vereniging.db");

            // optionsBuilder.UseSqlite("Data Source=Vereniging.db");
            // optionsBuilder.UseSqlServer("Server=localhost;Database=Vereniging;User ID=sa;Password=reallyStrongPwd123");

            // optionsBuilder.UseLoggerFactory(ConsoleLoggerFactory);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Lid>()
                .Property(x => x.Naam).IsRequired();
            modelBuilder.Entity<Lid>()
                .Property(x => x.Adres).IsRequired();

            modelBuilder.Entity<WedstrijdSpeler>()
                .Property(x => x.Bodsnummer).IsRequired();
            modelBuilder.Entity<WedstrijdSpeler>()
                .HasIndex(speler => speler.Bodsnummer).IsUnique();

            modelBuilder.Entity<Team>()
                .Property(x => x.TeamCode).IsRequired();
            modelBuilder.Entity<Team>()
                .HasIndex(x => x.TeamCode).IsUnique();

            modelBuilder.Entity<Wedstrijd>()
                .HasOne(p => p.Team)
                .WithMany(t => t.Wedstrijden);
            // .OnDelete(DeleteBehavior.SetNull);

//            modelBuilder.Query<ZaalRooster>()
//                .ToView("ZaalRooster");

//            modelBuilder.Entity<Training>()
//                .OwnsOne(x => x.StartTime);
//            modelBuilder.Entity<Training>()
//                .OwnsOne(x => x.EndTime);


//            modelBuilder.Entity<Training>()
//                .HasKey(training => new {training.TeamId, training.ZaalId});

//            modelBuilder.Entity<Wedstrijd>()
//                .HasKey(wedstrijd => new {wedstrijd.TeamId, wedstrijd.ZaalId});
        }
    }
}