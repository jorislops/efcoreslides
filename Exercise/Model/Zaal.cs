﻿using System.Collections.Generic;

namespace Exercise
{
    public class Zaal
    {
        public int ZaalId { get; set; }
        public string Naam { get; set; }
        public string Telnr { get; set; }
        public string Adres { get; set; }

        public ICollection<Training> Trainingen { get; set; }
        public ICollection<Wedstrijd> Wedstrijden { get; set; }

        public Zaal()
        {
            Trainingen = new List<Training>();
            Wedstrijden = new List<Wedstrijd>();
        }
    }
}