﻿using System;
using System.Collections.Generic;

namespace Exercise
{
    public class Lid
    {
        public int LidId { get; set; }
        public string Naam { get; set; }
        public string Adres { get; set; }
        public DateTime Geboortedag { get; set; }
        public Geslacht Geslacht { get; set; }
        public DateTime Toetreding { get; set; }

        public Team Traint { get; set; }

        public ICollection<Wedstrijd> GeflotenWedstrijden { get; set; }

        public Lid()
        {
            GeflotenWedstrijden = new List<Wedstrijd>();
        }
    }
}