﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Exercise
{
    public class Wedstrijd
    {
        public int WedstrijdId { get; set; }

        public int ZaalId { get; set; }
        public Zaal Zaal { get; set; }

        [Required]
        public int TeamId { get; set; }
        [Required]
        public Team Team { get; set; }

        public DateTime DateTime { get; set; }

        public ICollection<Boete> Boetes { get; set; }

        public int ScheidsrechterId { get; set; }
        public Lid Scheidsrechter { get; set; }

        public Wedstrijd()
        {
            Boetes = new List<Boete>();
        }
    }
}