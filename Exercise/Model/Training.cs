﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using NodaTime;

namespace Exercise
{
    public class Training
    {
        public int TrainingId { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }
        public int ZaalId { get; set; }
        public Zaal Zaal { get; set; }

        public DateTime Aanvang { get; set; }

        public DayOfWeek StartDay { get; set; }

        [NotMapped] public LocalTime StartTime { get; set; }

        public string StartTimeAsStr
        {
            get { return StartTime != null ? StartTime.ToString() : ""; }
            set
            {
                string[] parts = value.Split(":");
                int hour = Convert.ToInt32(parts[0]);
                int minutes = Convert.ToInt32(parts[1]);
                int seconds = Convert.ToInt32(parts[2]);

                StartTime = new LocalTime(hour, minutes, seconds);
            }
        }

        [NotMapped] public LocalTime EndTime { get; set; }

        public string EndTimeAsStr
        {
            get { return EndTime != null ? EndTime.ToString() : ""; }
            set
            {
                string[] parts = value.Split(":");
                int hour = Convert.ToInt32(parts[0]);
                int minutes = Convert.ToInt32(parts[1]);
                int seconds = Convert.ToInt32(parts[2]);

                EndTime = new LocalTime(hour, minutes, seconds);
            }
        }
    }
}