﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Exercise
{
    public class Boete
    {
        public int BoeteId { get; set; }
        public decimal Bedrag { get; set; }
        public int? WedstrijdSpelerId { get; set; }
        public WedstrijdSpeler WedstrijdSpeler { get; set; }
        public int WedstrijdId { get; set; }
        public Wedstrijd Wedstrijd { get; set; }

        [NotMapped]
        public bool IsBoeteVoorVereniging
        {
            get { return !WedstrijdSpelerId.HasValue; }
        }

        [NotMapped]
        public bool IsBoetVoorSpeler
        {
            get { return !IsBoeteVoorVereniging; }
        }

        public string Toelichting { get; set; }
    }
}