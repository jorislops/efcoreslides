﻿using System.Collections.Generic;

namespace Exercise
{
    public class WedstrijdSpeler : Lid
    {
        public int Bodsnummer { get; set; }
        public ICollection<Boete> Boetes { get; set; }

        public int TeamId { get; set; }
        public Team Team { get; set; }

        public WedstrijdSpeler() : base()
        {
            Boetes = new List<Boete>();
        }
    }
}