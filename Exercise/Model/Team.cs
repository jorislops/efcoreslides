﻿using System.Collections.Generic;

namespace Exercise
{
    public class Team
    {
        public int TeamId { get; set; }
        public string TeamCode { get; set; }
        public string Klasse { get; set; }

        public ICollection<Training> Trainingen { get; set; }
        public ICollection<Wedstrijd> Wedstrijden { get; set; }
        public ICollection<WedstrijdSpeler> Spelers { get; set; }

        public int TrainerId { get; set; }
        public Lid Trainer { get; set; }

        public Team()
        {
            Trainingen = new List<Training>();
            Wedstrijden = new List<Wedstrijd>();
            Spelers = new List<WedstrijdSpeler>();
        }
    }
}