using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using NUnit.Framework;

namespace Exercise
{
    public static class ExtensionMethodsExamples
    {
        public static string UppercaseFirstLetter(this string value)
        {
            if (value?.Length > 0)
            {
                value = char.ToUpper(value[0]) + value.Substring(1);
            }
            return value;
        }

        public static IEnumerable<string> UppercaseFirstLetter(this IEnumerable<string> values)
        {
            foreach (var value in values)
            {
                yield return value.UppercaseFirstLetter();
            }
        }

        public static IEnumerable<TSource> Filter<TSource>(this IEnumerable<TSource> values, Predicate<TSource> predicate)
        {
            foreach (var value in values)
            {
                if (predicate(value))
                {
                    yield return value;
                }
            }            
        }

        [Test]
        public static void CallExtensionMethods()
        {
            //"aap noot mies".Split(" ").Where();
            
            
            
            Console.WriteLine("aap noot mies".UppercaseFirstLetter());
            
            "aap noot mies".Split(" ").UppercaseFirstLetter()
                .ToList().ForEach(x => Console.WriteLine(x));

            "Aap noot Mies".Split(" ")
                .Filter(x => x.Length > 0 && char.IsUpper(x[0]))
                .ToList()
                .ForEach(x => Console.WriteLine(x));
        }
    }
}