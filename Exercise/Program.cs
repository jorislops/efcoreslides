﻿using System;
using System.Linq;
using Exercise.Queries;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using NUnit.Framework;

namespace Exercise
{
    class Program
    {
        [Test]
        public static void MainProgram()
        {
            using (var db = new VerenigingContext())
            {
                db.Database.EnsureDeleted();
                string script = db.Database.GenerateCreateScript();
                db.Database.EnsureCreated();

                if (!db.Teams.Any())
                {
                    var (leden, teams) = SeedDatabase.GenerateData();

                    db.AddRange(leden);
                    db.AddRange(teams);

                    db.SaveChanges();
                }

                QueryExamples.ScheidsVoorZaal(db, "Hettinger, Wisoky and Keeling Zaal");
                QueryExamples.TeamMetMeesteWedstrijden(db);
                //QueryExamples.RecreantenTeams(db);

                QueryExamples.RemoveLid(db);
            }

            // Console.ReadKey();
        }

        
    }
}