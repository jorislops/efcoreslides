﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Logging.Console;
using SQLitePCL;

namespace EFCoreSlides
{
    partial class Program
    {
        public class Student
        {
//            public Student()
//            {
//                Degrees = new List<Degree>();
//                CourseStudents = new List<CourseStudent>();   
//            }
            
            public int StudentId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            
            public ContactInfo ContactInfo { get; set; }
            
            public int StudyId { get; set; }
            public Study Study { get; set; }
            
            public ICollection<Degree> Degrees { get; set; }

            public ICollection<CourseStudent> CourseStudents { get; set; }
        }

        public class ContactInfo
        {
            public int ContactInfoId { get; set; }
            
            public int StudentId { get; set; }
            public Student Student { get; set; }
            
            public string PhoneNumber { get; set; }
            public string Address { get; set; }
        }

        public class Study
        {
//            public Study()
//            {
//                Students = new List<Student>();
//            }

            public int StudyId { get; set; }
            public string UniversityName { get; set; }
            public string Name { get; set; }
            public DateTime Completion { get; set; }
            
            public ICollection<Student> Students { get; set; }
        }
        
        public class Degree
        {
            public int DegreeId { get; set; }
            public string Name { get; set; }
            public DateTime Received { get; set; }
            
            public int StudentId { get; set; }
            public Student Student { get; set; }
        }
        
        public class Course
        {
            public Course()
            {
                CourseStudents = new List<CourseStudent>();
            }

            public int CourseId { get; set; }
            public string Name { get; set; }

            public ICollection<CourseStudent> CourseStudents { get; set; }
        }

        public class CourseStudent
        {
            public int CourseId { get; set; }
            public Course Course { get; set; }

            public int StudentId { get; set; }
            public Student Student { get; set; }
        }

        public class UniversityDbContext : DbContext
        {
            public DbSet<Student> Students { get; set; }
            public DbSet<Study> Studies { get; set; }
            public DbSet<Degree> Degrees { get; set; }
            public DbSet<Course> Courses { get; set; }

            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
                base.OnConfiguring(optionsBuilder);
                optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
                optionsBuilder.UseSqlite("Data Source=University.db");
            }

            protected override void OnModelCreating(ModelBuilder modelBuilder)
            {
                base.OnModelCreating(modelBuilder);
                modelBuilder.ApplyConfiguration(new CourseConfiguration());
                //ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
                
                //define key in many-to-many relationship
                modelBuilder.Entity<CourseStudent>()
                    .HasKey(x => new {x.CourseId, x.StudentId});

                //configure cascading in one-to-many relationship
                modelBuilder.Entity<Student>()
                    .HasMany(x => x.Degrees)
                    .WithOne(x => x.Student)
                    .OnDelete(DeleteBehavior.Cascade);

                //Shadow property
                modelBuilder.Entity<Student>().Property<DateTime>("LastModified");

                //Give every Entity a Shadow Property
                foreach (var entityType in modelBuilder.Model.GetEntityTypes())
                {
                    modelBuilder.Entity(entityType.Name).Property<DateTime>("LastModified");
                }
            }

            public override int SaveChanges()
            {
                //add shadow property logic
                foreach (var entry in ChangeTracker.Entries()
                    .Where(x => x.State == EntityState.Added || x.State == EntityState.Modified))
                {
                    entry.Property("LastModified").CurrentValue = DateTime.Now;
                }
                
                return base.SaveChanges();
            }
        }

        //configure the course
        public class CourseConfiguration : IEntityTypeConfiguration<Course>
        {
            public void Configure(EntityTypeBuilder<Course> builder)
            {
                builder.Property(x => x.Name).IsRequired(); 
            }
        }
        
        public class Sheets
        {
            public static void FillDatabase()
            {
                using (var db = new UniversityDbContext())
                {
                    var cs = new Study()
                    {
                        Name = "Computer Science"
                    };
                    
                    db.Students.Add(new Student()
                    {
                        FirstName = "Jan",
                        LastName = "Hendriks",
                        Study = cs,
                        ContactInfo = new ContactInfo()
                        {
                          Address  = "One way street 2",
                            PhoneNumber = "0031-234244242"
                        },
                        Degrees = new List<Degree>()
                        {
                            new Degree()
                            {
                                Name = "bachelor computer science", 
                                Received = new DateTime(2011, 6, 1)
                            },
                            new Degree()
                            {
                                Name = "bachelor mathematics",
                                Received = new DateTime(2008, 3, 1)
                            }
                        }
                    });
    
                    db.Students.Add(new Student()
                    {
                        FirstName = "Dave",
                        LastName = "de Graaf",
                        Study = cs,
                        Degrees = new List<Degree>()
                        {
                            new Degree()
                            {
                                Name = "bachelor computer science", 
                                Received = new DateTime(2012, 8, 3),
                            },
                        }
                    });
                    
                    db.Students.Add(new Student()
                    {
                        FirstName = "Jan Willem",
                        LastName = "de Boer",
                        Study = new Study()
                        {
                            Name = "Business ict"
                        }
                    });
    
                    db.SaveChanges();
                }
            }
            
            public static void ConnectedObjectUpdate()
            {
                Student studentToUpdate = null;
                using (var db = new UniversityDbContext())
                {
                    studentToUpdate = db.Find<Student>(1);
                    
                    studentToUpdate.FirstName += studentToUpdate.FirstName.First();

                    db.SaveChanges();    
                }
            }

            public static void DisconnectObjectUpdate()
            {
                Student studentToUpdate = null;
                using (var db = new UniversityDbContext())
                {
                    studentToUpdate = db.Find<Student>(1);
                }

                studentToUpdate.FirstName += studentToUpdate.FirstName.First();

                using (var db = new UniversityDbContext())
                {
                    db.Students.Update(studentToUpdate);
                    db.SaveChanges();
                }
            }

            public static void NoTracking()
            {
                using (var db = new UniversityDbContext())
                {
                    //disable tracking for all operations on context
                    db.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;

                    //no tracking for one query
                    db.Students.AsNoTracking();
                }
            }

            public static void DeleteStudent(int studentId)
            {
                using (var db = new UniversityDbContext())
                {
                    Student student = db.Students.FirstOrDefault(x => x.StudentId == studentId);
                    if (student != null)
                    {
                        db.Students.Remove(student);
                        db.SaveChanges();
                    }
                }
            }

            public static void UpdateDisconnectedStudent(int studentId)
            {
                Student student = null;
                     
                using (var db = new UniversityDbContext())
                {
                    student = db.Students
                        .Include(x => x.Degrees)
                        .SingleOrDefault(x => x.StudentId == studentId);
                }

                Debug.Assert(student != null, nameof(student) + " != null");
                
                student.FirstName = student.FirstName.First().ToString();
                student.Degrees.Add(new Degree() { Name = "New Degree"});
                student.Degrees.First().Name = student.Degrees.First().Name + "!";
                
                //indexing is not a good idea in db, because order of objects is not
                //fixed by sql (need order by clause)
                student.Degrees.Remove(student.Degrees.ElementAt(1));
                
                UpdateDisconnectedStudent(student);
            }

            public static void UpdateDisconnectedStudent(Student studentModel)
            {
                using (var db = new UniversityDbContext())
                {
                    db.Entry(studentModel).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

            public static void DisplayStudentForStudy()
            {
                using (var db = new UniversityDbContext())
                {
                    //var queryNotWorking = db.Studies; 
                    var query = db.Studies.Include(x => x.Students);
                    
                    foreach (var study in query)
                    {
                        Console.WriteLine($"Study: {study.Name}");
                        foreach (var student in study.Students)
                        {
                            Console.WriteLine($"\tstudent: {student.FirstName}");    
                        }
                    }
                }
            }

            public static void DisplayDegreeNamesByStudentName(string firstName, string lastName)
            {
                using (var db = new UniversityDbContext())
                {
                    var degrees = db.Degrees
                        .Where(x => x.Student.FirstName == firstName && x.Student.LastName == lastName)
                        .Select(x => x.Name);

                    Console.WriteLine($"Degrees for {firstName} {lastName}");
                    foreach (var degree in degrees)
                    {
                        Console.WriteLine($"Degree: {degree}");
                    }
                }
            }

            public static void DisplayNumberOfDegreesPerStudent()
            {
                //to show projection select(x => ...)

                using (var db = new UniversityDbContext())
                {
                    var studentsDegreeCount = db.Students
                        .Select(x => new
                        {
                            Name = x.FirstName + " " + x.LastName,
                            DegreeCount = x.Degrees.Count(),
                            DegreeCountBefore2010 = 
                                x.Degrees.Count(d => d.Received < new DateTime(2010, 1, 1))
                        });

                    foreach (var r in studentsDegreeCount)
                    {
                        Console.WriteLine($"{r.Name} Number of Degrees {r.DegreeCount} " +
                                          $"Number of degrees before 2000: {r.DegreeCountBefore2010}");
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            using (var db = new UniversityDbContext())
            {
                db.Database.EnsureDeleted();
                db.Database.EnsureCreated();
            }   
            
            Sheets.FillDatabase();

            Sheets.DisplayDegreeNamesByStudentName("Jan", "Hendriks");
            Sheets.DisplayNumberOfDegreesPerStudent();
            Sheets.DisplayStudentForStudy();
            
            
            
            LoadRelatedData.QueryLoadWithInclude();
            LoadRelatedData.QueryLoadWithIncludeThen();
            LoadRelatedData.QueryLoadWithProjection();
            LoadRelatedData.QueryLoadRelatedDataAfterQuery();
            LoadRelatedData.QueryLoadRelatedDataInNavigationProperty();
            
            
            Sheets.ConnectedObjectUpdate();
            Sheets.DisconnectObjectUpdate();
            
            Sheets.UpdateDisconnectedStudent(1);
            //Sheets.DeleteStudent(1);
            

            using (var db = new UniversityDbContext())
            {
                var csStudents = db.Students.Where(x => x.Study.Name == "Computer Science");
                foreach (var csStudent in csStudents)
                {
                    Console.WriteLine($"Name: {csStudent.LastName}, {csStudent.FirstName.First()}");
                    //Do lots of work
                }

                var csStudents2 = db.Students.Where(x => x.Study.Name == "Computer Science").ToList();

                //exception if no student exists with Lastname equals "Lops"
                //Student s1 = db.Students.Single(x => x.LastName == "Lops");
                
                
                Student s2 = db.Students.SingleOrDefault(x => x.LastName == "Lops");
                if (s2 == null)
                {
                    //do something
                }
            }
            
            using (var db = new UniversityDbContext())
            {
                var csStudents = from s in db.Students
                    where s.Study.Name == "Computer Science"
                    select s;
                foreach (var csStudent in csStudents)
                {
                    Console.WriteLine($"Name: {csStudent.LastName}, {csStudent.FirstName.First()}");
                }
            }

//            using (var db = new UniversityDbContext())
//            {
//                Student student = db.Students.Find(1);
//
//                db.Students.Remove(student);
//                db.SaveChanges();
//            }

            using (var db = new UniversityDbContext())
            {
                Student student = db.Students
                    .Include(x => x.Degrees)
                    .Single(x => x.StudentId == 1);

//                foreach (var degree in student.Degrees)
//                {
//                    db.Degrees.Remove(degree);
//                }
                db.Students.Remove(student);
                db.SaveChanges();
            }
            
            Console.ReadKey();
        }
    }


}