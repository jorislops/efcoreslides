using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace EFCoreSlides
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        
        [Required, MinLength(3), MaxLength(50)]
        public string FirstName { get; set; }
        [Required, MinLength(3), MaxLength(50)]
        public string LastName { get; set; }
        
        [NotMapped]
        public string FullName {
            get { return $"{FirstName} {LastName}"; } 
        }
        
        [Required, EmailAddress]
        public string Email { get; set; }
        
        [Phone]
        public string Phone { get; set; }
        
        [System.ComponentModel.DataAnnotations.Range(1, 5)]
        public int WorkdayCountInWeek { get; set; }
        
        [Required, System.ComponentModel.DataAnnotations.Range(0, 5000)]
        public int Salary { get; set; }
        
        [Url, Required]
        public string IntranetPage { get; set; }
        
        public DateTime StartDate { get; set; }
    }

    public class EmployeeContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        
        // public static readonly LoggerFactory ConsoleLoggerFactory =
        //     new LoggerFactory(new []{new ConsoleLoggerProvider((category, level) => 
        //         category == DbLoggerCategory.Database.Command.Name && level == LogLevel.Information, true
        //         ) });
        
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=Employee.db");
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .ToTable("Emp");
            
            modelBuilder.Entity<Employee>()
                .Property(x => x.FirstName)
                .IsRequired().HasMaxLength(50);
    
            modelBuilder.Entity<Employee>()
                .Property(x => x.Phone)
                .HasColumnName("PhoneNumber");
                
            
            base.OnModelCreating(modelBuilder);
        }
    }

//    public class EmployeeProgram
//    {
//        [Test]
//        public static void Example1()
//        {
//            using (var db = new EmployeeContext())
//            {
//                db.Database.EnsureDeleted();
//                db.Database.EnsureCreated();
//
//                db.Employees.Add(new Employee()
//                {
//                    Email = "jorislops@gmail.com",
//                    FirstName = "Joris",
//                    LastName = "Lops",
//                    IntranetPage = "http://www.nhl.nl/jorislops",
//                    Phone = "06161632",
//                    Salary = 1000,
//                    StartDate = DateTime.Today,
//                    WorkdayCountInWeek = 4
//                });
//                db.SaveChanges();
//
//                var employees = db.Employees.FromSql("SELECT * FROM Emp");
//                foreach (var employee in employees)
//                {
//                    Console.WriteLine(employee.LastName);
//                }
//            }
//        }
//    }

//    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
//    {
//        public void Configure(EntityTypeBuilder<Employee> builder)
//        {
//            builder.Property(x => x.)
//        }
//    }
    
    
}