﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace EFCoreSlides
{
    public class LoadRelatedData
    {
        public static void QueryLoadWithInclude()
        {
            using (var db = new Program.UniversityDbContext())
            {
                var students = db.Students
                    .Include(x => x.Degrees);

                foreach (var student in students)
                {
                    string degreeList = string.Join(',', student.Degrees.Select(x => x.Name));

                    Console.WriteLine($"{student.LastName}, {student.FirstName.First()}: {degreeList} ");
                }
            }
        }

        public static void QueryLoadWithIncludeThen()
        {
            using (var db = new Program.UniversityDbContext())
            {
                var students = db.Students
                    .Include(x => x.CourseStudents)
                    .ThenInclude(x => x.Course)
                    .Include(x => x.ContactInfo);

                foreach (var student in students)
                {
                    Console.WriteLine(new string('-', 20));
                    Console.WriteLine(
                        $"{student.LastName}, {student.FirstName.First()} Phone: {student?.ContactInfo?.PhoneNumber} ");
                    foreach (var cs in student.CourseStudents)
                    {
                        Console.WriteLine(cs.Course.Name);
                    }

                    Console.WriteLine(new string('-', 20));
                }
            }
        }

        public static void QueryLoadWithProjection()
        {
            using (var db = new Program.UniversityDbContext())
            {
                var studentDegrees = db.Students
                    .Select(x => new
                    {
                        x.LastName, x.FirstName, x.Degrees

                        ,DegreesBefore2014 = x.Degrees.Where(d => d.Received < new DateTime(2014, 1, 1))
                    });

                foreach (var sd in studentDegrees)
                {
                    string degreeList = string.Join(',', sd.Degrees.Select(x => x.Name));

                    Console.WriteLine($"{sd.LastName}, {sd.FirstName.First()}: {degreeList} ");
                }
            }
        }

        public static void QueryLoadRelatedDataInNavigationProperty()
        {
            using (var db = new Program.UniversityDbContext())
            {
                // var onlyStudentWithOldDegree =
                //     db.Students
                //         .Include(x => x.Degrees)
                //         .Where(x => x.Degrees.Any(w => w.Received < new DateTime(2010, 1, 1)))
                //         .ToList();

                //this is not possible, in ef 6 core it is!
                var studentWithDegreeBefore2014 =
                    db.Students.Include(x => 
                        x.Degrees.Where(d => d.Received < new DateTime(2010,1,1))
                    ).ToList();

                foreach (var student in studentWithDegreeBefore2014)
                {
                    Console.WriteLine(student.FirstName);
                }
                
                //only solution (before ef core 6), only one record for demonstration 
                var students = db.Students.Where(x => x.LastName == "Hendriks");

                foreach (Program.Student student in students)
                {
                    //load study of Student 
                    db.Entry(student).Reference<Program.Study>(x => x.Study).Load();

                    if (student.Study.Name == "Computer Science")
                    {
                        Console.WriteLine($"{student.LastName}");

                        db.Entry(student).Collection(x => x.Degrees)
                            .Query()
                            .Where(x => x.Received < new DateTime(2014, 1, 1))
                            .Load();

                        foreach (var degree in student.Degrees)
                        {
                            Console.WriteLine($"{degree.Name}");
                        }
                    }
                }
            }
        }

        public static void QueryLoadRelatedDataAfterQuery()
        {
            using (var db = new Program.UniversityDbContext())
            {
                var students = db.Students;
                foreach (Program.Student student in students)
                {
                    if (student.LastName == "Hendriks")
                    {
                        db.Entry(student).Reference<Program.Study>(x => x.Study).Load();

                        if (student.Study.Name == "Computer Science")
                        {
                            Console.WriteLine($"{student.LastName}");

                            db.Entry(student).Collection(x => x.Degrees)
                                .Query()
                                .Where(x => x.Received < new DateTime(2014, 1, 1))
                                .Load();

                            foreach (var degree in student.Degrees)
                            {
                                Console.WriteLine($"{degree.Name}");
                            }
                        }
                    }
                }
            }
        }
    }
}